terraform {
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = "1.54.1"
    }
  }
  backend "http" {
      address = "https://gitlab.com/api/v4/projects/55721809/terraform/state/vpn_state"
      lock_address = "https://gitlab.com/api/v4/projects/55721809/terraform/state/vpn_state/lock"
      unlock_address = "https://gitlab.com/api/v4/projects/55721809/terraform/state/vpn_state/lock"
      lock_method = "POST"
      unlock_method = "DELETE"
      retry_wait_min = 5
  }
}

# Depuis Gitlab -> Opération -> Etats Terraform
# export GITLAB_ACCESS_TOKEN=<YOUR-ACCESS-TOKEN>
# export TF_STATE_NAME=default
# terraform init \
#     -backend-config="address=https://gitlab.com/api/v4/projects/55721809/terraform/state/$TF_STATE_NAME" \
#     -backend-config="lock_address=https://gitlab.com/api/v4/projects/55721809/terraform/state/$TF_STATE_NAME/lock" \
#     -backend-config="unlock_address=https://gitlab.com/api/v4/projects/55721809/terraform/state/$TF_STATE_NAME/lock" \
#     -backend-config="username=Ronach" \
#     -backend-config="password=$GITLAB_ACCESS_TOKEN" \
#     -backend-config="lock_method=POST" \
#     -backend-config="unlock_method=DELETE" \
#     -backend-config="retry_wait_min=5"

# https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html
# https://developer.hashicorp.com/terraform/language/settings/backends/http

