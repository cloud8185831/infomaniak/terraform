# Une data source permet de récupérer des informations spécifiques sur des ressources déjà existantes dans le cloud. Ces informations peuvent inclure des 
# détails tels que les ID, les adresses IP, les noms, les tags, etc.
data "openstack_networking_subnet_ids_v2" "ext_subnets" {
  network_id = var.network_external_id
}

resource "openstack_compute_instance_v2" "openvpn" {
  name            = "openvpn"
  image_id        = var.instance_image_id
  flavor_name     = var.instance_flavor_name
  metadata        = var.metadatas
  security_groups = [openstack_networking_secgroup_v2.openvpn.name, openstack_networking_secgroup_v2.ssh.name, "default"]
  key_pair        = openstack_compute_keypair_v2.ssh_public_key.name
  #user_data       = "#cloud-config\nhostname: instance_1.example.com\nfqdn: instance_1.example.com"
  network {
    name = var.network_internal_dev
  }
  # Conditionne la création au fait que ces ressources soient d'abord créées
  depends_on = [openstack_networking_subnet_v2.network_subnet, openstack_networking_secgroup_rule_v2.secgroup_openvpn_rule_tcp_v4, openstack_networking_secgroup_rule_v2.secgroup_openvpn_rule_udp_v4 ]
}

# Deprecated. Use "openstack_networking_floatingip_associate_v2" resource instead.
# Associe l'IP flottante à notre instance openvpn
resource "openstack_compute_floatingip_associate_v2" "fip_assoc" {
  floating_ip = openstack_networking_floatingip_v2.floatip_1.address
  instance_id = openstack_compute_instance_v2.openvpn.id
}

resource "openstack_networking_floatingip_v2" "floatip_1" {
  pool       = var.network_external_name
  subnet_ids = data.openstack_networking_subnet_ids_v2.ext_subnets.ids
}

# # Documentation
# resource "openstack_networking_port_v2" "port_1" {
#   network_id = "a5bbd213-e1d3-49b6-aed1-9df60ea94b9a"
# }

# resource "openstack_networking_floatingip_associate_v2" "fip_1" {
#   floating_ip = "1.2.3.4"
#   port_id     = openstack_networking_port_v2.port_1.id
# }