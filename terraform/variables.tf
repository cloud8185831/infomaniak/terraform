# ID du réseau ext-floating1 définit par défaut chez Infomaniak
variable "network_external_id" {
  type         = string
  default      = "0f9c3806-bd21-490f-918d-4a6d1c648489"
  description = "Interface externe pour y fixer la floating IP"
  sensitive = false
}

variable "network_external_name" {
  type    = string
  default = "ext-floating1"
  description = "Nom du réseau ouvert sur Internet, définit par défaut chez Infomaniak"
  sensitive = false
}

variable "network_internal_dev" {
  type         = string
  default      = "internal_dev"
  description = "Nom du réseau interne que l'on crée"
  sensitive = false
}

variable "network_subnet_cidr" {
  type = string
  default = "10.0.1.0/24"
  description = "254 IPs privées disponible dans le sous-réseau interne"
  sensitive = false
}

variable "ssh_public_key_default_user" {
  type = string
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5EsSwdJy1R3NgFPZwqPmyjqbhstz39KMfmMXzPdY4+bvwPmnYnT07xziN4mK1AAh+sbCUi2ZlqLPoff50IxD2Wf65Mdx9C3q+iFuB1Km33/wt1ZkS/oM6vTkBbD9DTMvyExnVOZXrZVymAsx6bXNEb1QSIeEMmpx0D2yUJv9nrriil+xz1LLUGvOqixqC3YqHW4njxXEh6BQotAOfo9zzORk3z+qwlQYtbt8SGbSfdL3rnp5YUj6EJULR17dXQ7O+FlC41TA2gCSe8I35C9N4fwuGk9ZqIgbJ5w5kLCqe0iHMRDQdzFdeTEkuTviNXRgOtJ5YaU5weqoy+vk3o4Yt Generated-by-Nova" 
  description = "Clé SSH publique"
  sensitive = false
}

variable "instance_security_groups" {
  type    = list(any)
  default = ["default"]
  description = ""
  sensitive = false
}

variable "instance_image_id" {
  type = string
  default = "cdf81c97-4873-473b-b0a3-f407ce837255"
  description = "ID de la distribution choisit"
  sensitive = false
}

variable "instance_flavor_name" {
  type = string
  default = "a1-ram2-disk20-perf1"
  description = "Nom du gabarit (vCPU,RAM,disk) de la distribution choisit"
  sensitive = false
}

variable "metadatas" {
  type = map(string)
  default = {
    "environment" = "dev",
    "custom" = "vpn"
  }
  description = "Métadonnées qu'on peut apposer sur les ressources"
  sensitive = false
}