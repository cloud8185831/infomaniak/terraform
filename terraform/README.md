# Introduction
Pour l'authentification à OpenStack on utilise le fichier openrc.sh qui définit des variables d'environnement pour réaliser l'authentification.
Très mauvaise pratique de mettre dans provider "openstack" { } car on veut versionner notre infrastructure en poussant le code Terraform sur GitLab...

Un provider est un connecteur (cf schéma d'architecture) qui permet à Terraform de communiquer avec l'API d'OpenStack.  

Il va y avoir 2 states, un pour openvpn et un autre pour le reste de l'infrastructure.

## Commandes utiles
```
terraform init --> crée .terraform/ en téléchargeant le provider  
terraform plan -var-file=prod.tfvars --> opportun pour du multi env  
terraform apply -auto-approve --> crée terraform.tfstate  
terraform state list  
terraform state show openstack_compute_instance_v2.serveurs_web  
terraform destroy -auto-approve  
```
## Installation du client OpenStack et commandes utiles
```
python3 -m venv venv
source venv/bin/activate
pip3 install openstacksdk python-openstackclient
source openrc.sh

openstack keypair create --public-key ~/.ssh/loann_ssh_rsa.pub loann_ssh
openstack server create --key-name loann_ssh --flavor a1-ram2-disk20-perf1 --network ext-net1 --image "Debian 12 bookworm" instance1
openstack security group rule create --ingress --protocol tcp --dst-port 22 --ethertype IPv4 default
openstack project list
openstack server list
openstack server show
openstack server delete
openstack keypair delete
```
## Commandes utiles pour Ansible
```
chmod 600 ~/.ssh/loann_ssh_rsa
ssh-add ~/.ssh/loann_ssh_rsa
eval $(ssh-agent)
ssh-add -l
ssh-copy-id -i ~/.ssh/loann_ssh_rsa.pub <user>@<IP>
ansible -i inventory.yml all -u debian --private-key=~/.ssh/loann_ssh_rsa -m ping
ansible -i inventory.yml all -u debian -m ping
ansible -i inventory.yml all -b -K/-k -u debian -m command -a "ls /root"
ansible-galaxy init roles/openvpn
ansible-playbook -i envs/dev/ -l openvpn -u debian playbook.yml
```
